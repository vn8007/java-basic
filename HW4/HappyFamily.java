package HW4;

public class HappyFamily {
    public static void main(String[] args) {
        Family family = new Family();
        family.setMother(new Human());
        family.getMother().setName("Anna");
        family.setFather(new Human("John", "Pupkin"));
        family.setChildren(new Human[0]);
        family.setPet(new Pet());
        family.getMother().setScedule(new String[][] {
                {"Sunday","do home work"},
                {"Monday", "go to courses; watch a film"},
                {"Tuesday", "go to the office"},
                {"Wednesday", "do my exercises"},
                {"Thursday", "drink beer"},
                {"Friday", "party day"},
                {"Saturday", "seek a doctor"}
        });
        family.counterFamily(family.getChildren());
        family.addChild(new Human("Vasia", "Pupkin"));
        family.counterFamily(family.getChildren());
        family.addChild(new Human("Kolia", "Pupkin"));
        family.counterFamily(family.getChildren());
        family.deleteChild(1);
        family.counterFamily(family.getChildren());
        family.deleteChild(0);
        family.deleteChild(4);
        family.counterFamily(family.getChildren());
    }
}