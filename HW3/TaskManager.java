package HW3;

import java.util.Scanner;

public class TaskManager {
    public static void main(String[] args) {
        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to the office";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "do my exercises";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "drink beer";
        scedule[5][0] = "Friday";
        scedule[5][1] = "party day";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "seek a doctor";

        String exit = "Exit";
        String userDay;
        while (true) {
            System.out.println("Please, input the day of the week:");
            Scanner scan = new Scanner(System.in);
            userDay = scan.nextLine();
            if (userDay.toLowerCase().trim().equals((exit).toLowerCase().trim())) {
                    System.out.println("Goodbye!!!");
                    System.exit(0);
            }
            userDay = userDay.toLowerCase().trim();
            userDay = userDay.substring(0, 1).toUpperCase() + userDay.substring(1);
            CheckInput.checkInput(userDay,scedule);
            DoTask.doTask(userDay, scedule);
        }
    }
}