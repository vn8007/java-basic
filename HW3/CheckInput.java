package HW3;

public class CheckInput {
    public static void checkInput (String str, String[][]matrix){
        int counter = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (str.equals(matrix[i][j])) {
                    counter++;
                }
            }
        }
        if(counter == 0) {
            System.out.println("Sorry, I don't understand you, please try again.");
        }
    }
}
