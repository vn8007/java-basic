package HW12;

import HW12.layered.controller.FamilyController;
import HW12.layered.dao.CollectionFamilyDao;
import HW12.layered.service.FamilyService;
import HW12.layered.service.FamilyServiceDefault;

public class HappyFamily {
    public static void main(String[] args)  {
        FamilyService familyService = new FamilyServiceDefault();
        FamilyController familyController = new FamilyController();
        familyController.setFamilyService(familyService);
        CollectionFamilyDao familyDao = new CollectionFamilyDao();
        familyService.setCollectionFamilyDao(familyDao);
        familyController.printMenu();
    }
}