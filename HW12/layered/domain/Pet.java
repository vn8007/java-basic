package HW12.layered.domain;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public abstract class Pet {
    private Set<String> habits = new HashSet<>();
    private Specie specie = Specie.UNKNOWN;
    private  String nickname;
    private int age;
    private int trickLevel;

    public Specie getSpecies() {
        return specie;
    }
    public Set<String> getHabits() {
        return habits;
    }

    public void setNickname(String nickname){
        this.nickname = nickname;
    }
    public String getNickname(){
        return nickname;
    }
    public void setAge(int age){
        this.age = age;
    }
    public int getAge(){
        return age;
    }
    public void setTrickLevel(int trickLevel){
        this.trickLevel = trickLevel;
    }
    public int getTrickLevel(){
        return trickLevel;
    }

    void eat(){
        System.out.println("I am eating!");
    }

    abstract void respond();

    public String prettyFormat() {
        return "{species =" + getSpecies() +", nickname =" + this.nickname +
                ", age="+ this.age + ", trickLevel=" + this.trickLevel+ ", habits="+ getHabits() +"}";
    }

        @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;
        Pet pet = (Pet) o;
        return getTrickLevel() == pet.getTrickLevel() &&
                Objects.equals(getNickname(), pet.getNickname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNickname(), getTrickLevel());
    }

    @Override
    public String toString() {
        return  "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                '}';
    }

    public Pet(){};

    public Pet(String nickname, int age, int trickLevel) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }



}