package HW12.layered.domain;

public class Fish extends Pet {

    private Specie specie = Specie.FISH;

    public Specie getSpecies() {
        return specie;
    }

    void respond(){
        System.out.println("Hello. My name is "+ getNickname() +". I miss you!");
    }

}
