package HW12.layered.domain;

final public class Woman extends Human {
    public Woman(String name, String surname, String birthDate, int iq) {
    super(name, surname, birthDate, iq);
}

    public void makeup(){
        System.out.println("makeup");
    }

    public void greetPet() {
        System.out.println("Hello");
    }
}
