package HW12.layered.domain;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Human implements Serializable {
    private Map<String, String> scedule = new HashMap<>();
    private String name;
    private String surname;
    private long birthDate;
    private int age;
    private int iq;

    public void setBirthDate(String date){
        try {
            this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(date).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getBirthDate() {
       return new SimpleDateFormat("dd/MM/yyyy").format(new Date (birthDate));
    }

    public Map<String, String> getScedule() {
        return scedule;
    }

    public void setScedule(Map<String, String> scedule) {
        this.scedule = scedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void greetPet() {
        System.out.println("Some");
    }

    public void describeAge(){
        try{
            LocalDate currentDate = LocalDate.now();
            String pattern = "dd/MM/yyyy";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            LocalDate localDate = LocalDate.parse(getBirthDate(), formatter);
            int years = Period.between(localDate, currentDate).getYears();
            int months = Period.between(localDate, currentDate).getMonths();
            int days = Period.between(localDate, currentDate).getDays();
            System.out.println(getName() + " age " + years +" years, " + months + " months, " + days + " days");

        }catch(Exception e){
            System.out.println(e);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return  getIq() == human.getIq() &&
                Objects.equals(getName(), human.getName()) &&
                Objects.equals(getSurname(), human.getSurname());
    }

    public Human() {
    }


    public Human(String name) {
        this.name = name;
    }

    public Human(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Human(String name, String surname, String birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        setBirthDate(birthDate);
        this.iq = iq;
    }
    public String prettyFormat(){
       return " {name = " + this.name + ", surname = " + this.surname + ", birthDate = " + getBirthDate() +
               ", iq = " + this.iq + ", schedule = " + getScedule() +"}";
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getName(), getSurname(), getIq());
        return result;
    }

    @Override
    public String toString() {
        return "Human{" +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age='" + age + '\'' +
                ", birthDate='" + getBirthDate() + '\'' +
                '}';
    }
}