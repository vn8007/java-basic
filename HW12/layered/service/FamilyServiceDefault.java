package HW12.layered.service;

import HW12.layered.domain.Human;
import HW12.layered.domain.Man;
import HW12.layered.domain.Pet;
import HW12.layered.domain.Woman;
import HW12.layered.dao.CollectionFamilyDao;
import HW12.layered.domain.Family;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class FamilyServiceDefault implements FamilyService {
    private CollectionFamilyDao familyDao;
    public Scanner scanner = new Scanner(System.in);

    @Override
    public void setCollectionFamilyDao(CollectionFamilyDao familyDao){
        this.familyDao = familyDao;
    }

    @Override
    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }
    @Override
    public void displayAllFamilies() {
        familyDao.getAllFamilies().forEach(Family::prettyFormat);
    }
    @Override
    public void getFamiliesBiggerThen(int i) {
        List <Family> sorted = new ArrayList<>();
        familyDao.getAllFamilies().forEach(x -> {
            if(x.counterFamily() > i) {
                sorted.add(x);
            }
        });
        sorted.forEach(Family::prettyFormat);
    }
    @Override
    public void getFamiliesLessThen(int i) {
        List <Family> sorted = new ArrayList<>();
        familyDao.getAllFamilies().forEach(x -> {
            if(x.counterFamily() < i) {
                sorted.add(x);
            }
        });
        sorted.forEach(Family::prettyFormat);
    }
    @Override
    public void countFamiliesWithMemberNumber(int i) {
        List <Family> sorted = new ArrayList<>();
        familyDao.getAllFamilies().forEach(x -> {
            if(x.counterFamily() == i) {
                sorted.add(x);
            }
        });
        System.out.println("Families With Member Number " + i + " person is " + sorted.size());
            }
    @Override
    public void createNewFamily(Man man, Woman woman) {
        Family family = new Family();
        familyDao.saveFamily(family);
        family.setMother(woman);
        family.setFather(man);
    }
    @Override
    public void deleteFamilyByIndex(int index) {
        familyDao.deleteFamilyByIndex(index);
    }
    @Override
    public void bornChild(Family family, String name) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        for (int j = 0; j<getAllFamilies().size(); j++){
            if(getAllFamilies().get(j) == family)
            {getAllFamilies().get(j).getChildren().add(new Human(name,
                    getAllFamilies().get(j).getFather().getSurname(), dateFormat.format(date), 0));}
        }
    }
    @Override
    public void adoptChild(int index, Human human) {
        getFamilyById(index).getChildren().add(human);
    }
    @Override
    public void deleteChildrenOlderThen(int i) {
        familyDao.getAllFamilies().forEach(x ->{
            try {
                x.getChildren().forEach(y ->{
                    if(y.getAge() > i){
                        x.getChildren().remove(y);
                    }
                });
            }
            catch(Throwable e)
            {
//                System.out.println(e.getMessage());
            }
        });
    }
    @Override
    public int count() {
        if (this.familyDao == null){
            return 0;
        }
        else
         return familyDao.getAllFamilies().size();
    }
    @Override
    public Family getFamilyById(int id) {
        return familyDao.getAllFamilies().get(id);
    }
    @Override
    public void getPets(int index) {
        System.out.println(familyDao.getAllFamilies().get(index).getHomePets());
    }

    @Override
    public void addPet(int index, Pet pet) {
        familyDao.getAllFamilies().get(index).getHomePets().add(pet);
    }

    @Override
    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    @Override
    public void displayFamiliesBiggerThen() {
        System.out.println("enter number");
        int numb = scanner.nextInt();
        getFamiliesBiggerThen(numb);
    }

    @Override
    public void displayFamiliesLessThen() {
        System.out.println("enter number");
        int numb = scanner.nextInt();
        getFamiliesLessThen(numb);
    }

    @Override
    public void displayFamiliesWithMemberNumber() {
        System.out.println("enter number");
        int numb = scanner.nextInt();
        countFamiliesWithMemberNumber(numb);
    }

    @Override
    public void deleteFamilyByIndexMany() {
        System.out.println("enter number");
        int numb = scanner.nextInt();
        deleteFamilyByIndex(numb);
    }

    @Override
    public void menuDeleteChildrenOlderThen() {
        System.out.println("enter number");
        int numb = scanner.nextInt();
        deleteChildrenOlderThen(numb);
    }

    @Override
    public void menuCreateNewFamily() {
        System.out.println("Enter mother name");
        String nameMother = scanner.nextLine();
        System.out.println("Enter mother surname");
        String surnameMother = scanner.nextLine();
        System.out.println("Enter mother's year of birth (YYYY)");
        int yearBirthMother = scanner.nextInt();
        System.out.println("Enter mother's birth month (MM)");
        int monthBirthMother = scanner.nextInt();
        System.out.println("Enter mother's birth day (DD)");
        int dayBirthMother = scanner.nextInt();
        System.out.println("Enter mother's iq");
        int iqMother = scanner.nextInt();

        System.out.println("Enter father name");
        String nameFather = scanner.nextLine();
        System.out.println("Enter surfather name");
        String surnameFather = scanner.nextLine();

        System.out.println("Enter father's year of birth (YYYY)");
        int yearBirthFather = scanner.nextInt();
        System.out.println("Enter father's birth month (MM)");
        int monthBirthFather = scanner.nextInt();
        System.out.println("Enter father's birth day (DD)");
        int dayBirthFather = scanner.nextInt();
        System.out.println("Enter father's iq");
        int iqFather = scanner.nextInt();

        ArrayList<Integer> birthdayMother = new ArrayList<>();
        birthdayMother.add(dayBirthMother);
        birthdayMother.add(monthBirthMother);
        birthdayMother.add(yearBirthMother);
        String birthdayMotherStr = birthdayMother.stream().map(Object::toString).collect(Collectors.joining("/"));

        ArrayList<Integer> birthdayFather = new ArrayList<>();
        birthdayFather.add(dayBirthFather);
        birthdayFather.add(monthBirthFather);
        birthdayFather.add(yearBirthFather);
        String birthdayFatherStr = birthdayFather.stream().map(Object::toString).collect(Collectors.joining("/"));

        createNewFamily(new Man(nameFather, surnameFather, birthdayFatherStr, iqFather),
                new Woman(nameMother, surnameMother, birthdayMotherStr, iqMother));
    }
    public String birthdayMaker(){
        int day = 1 + (int)(Math.random()*31);
        int month = 1 + (int)(Math.random()*12);
        int year = 1990 + (int)(Math.random()*12);

        ArrayList<Integer> birthdayChild = new ArrayList<>();
        birthdayChild.add(day);
        birthdayChild.add(month);
        birthdayChild.add(year);
        String birthdayStr = birthdayChild.stream().map(Object::toString).collect(Collectors.joining("/"));

        return birthdayStr;
    }

    public int iqMaker(){
        return 1 + (int)(Math.random()*99);
    }

    public String stringMaker(){
        int length = 5;
        Random r = new Random();
        return r.ints(58, 122)
                .filter(i -> (i < 57 || i > 65) && (i < 90 || i > 97))
                .mapToObj(i -> (char) i)
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
    }

    public void familyMaker(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        for (int i = 0; i<4; i++){
            String surname = stringMaker();
            createNewFamily(new Man(stringMaker(), surname, birthdayMaker(), iqMaker()),
                    new Woman(stringMaker(),  surname, birthdayMaker(),iqMaker()));
            for (int j =0; j < 4; j++){
                getFamilyById(i).getChildren().add(new Human(stringMaker(),
                        surname, dateFormat.format(date), 0));
            }
        }
    }
}
