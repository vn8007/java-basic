package HW12.layered.service;

import HW12.layered.domain.Human;
import HW12.layered.domain.Man;
import HW12.layered.domain.Pet;
import HW12.layered.domain.Woman;
import HW12.layered.dao.CollectionFamilyDao;
import HW12.layered.domain.Family;

import java.util.List;

public interface FamilyService {
    void setCollectionFamilyDao(CollectionFamilyDao familyDao);
    List<Family> getAllFamilies();
    void displayAllFamilies();
    void getFamiliesBiggerThen(int i);
    void getFamiliesLessThen(int i);
    void countFamiliesWithMemberNumber(int i);
    void createNewFamily(Man man, Woman woman);
    void deleteFamilyByIndex(int index);
    void bornChild(Family family, String name);
    void adoptChild(int i, Human human);
    void deleteChildrenOlderThen(int i);
    int count();
    Family getFamilyById(int id);
    void getPets(int index);
    void addPet(int index, Pet pet);
    void saveFamily(Family family);
    void displayFamiliesBiggerThen();
    void displayFamiliesLessThen();
    void displayFamiliesWithMemberNumber();
    void deleteFamilyByIndexMany();
    void menuDeleteChildrenOlderThen();
    void menuCreateNewFamily();
    String birthdayMaker();
    int iqMaker();
    String stringMaker();
    void familyMaker();
}
