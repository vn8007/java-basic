package HW12.layered.dao;

import HW12.layered.domain.Family;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    public static final String FILE_NAME = "serialized.obj";
    private File file = new File(FILE_NAME);
    private List<Family> collectionFamilyDao = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return new ArrayList<>(collectionFamilyDao);
    }

    @Override
    public void getFamilyByIndex(int index) {
            if(index<0 || index>=collectionFamilyDao.size()){
                System.out.println("Incorrect index");
            }
            else {
                System.out.println(collectionFamilyDao.get(index));
            }
    }

    @Override

    public boolean deleteFamilyByIndex(int index) {
        if(index<0 || index>=collectionFamilyDao.size()) {
            return false;
        }
        else {
            collectionFamilyDao.remove(index);
            if(file.exists()){
                try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
                    oos.writeObject(this.collectionFamilyDao);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return true;
        }
    }

    @Override
    public boolean deleteFamily(Family family) {
        if(collectionFamilyDao.contains(family)){
            collectionFamilyDao.remove(family);
            if(file.exists()){
                try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
                    oos.writeObject(this.collectionFamilyDao);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return true;
        }
        else
            return false;
    }

    @Override
    public void saveFamily(Family family) {
        collectionFamilyDao.add(family);
        if(file.exists()){
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
                oos.writeObject(this.collectionFamilyDao);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public CollectionFamilyDao() {
        if (file.exists()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                this.collectionFamilyDao = (ArrayList<Family>)ois.readObject();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}