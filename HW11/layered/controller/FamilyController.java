package HW11.layered.controller;

import HW11.Human;
import HW11.Man;
import HW11.Pet;
import HW11.Woman;
import HW11.layered.domain.Family;
import HW11.layered.service.FamilyService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class FamilyController {
    private FamilyService familyService;
    private Scanner scanner = new Scanner(System.in);

    public void setFamilyService(FamilyService familyService){
        this.familyService = familyService;
    }
    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }
    public void displayAllFamilies() {
        familyService.getAllFamilies().forEach(Family::prettyFormat);
    }
    public void getFamiliesBiggerThen(int i) {
        List <Family> sorted = new ArrayList<>();
            familyService.getAllFamilies().forEach(x -> {
                if(x.counterFamily() > i) {
                    sorted.add(x);
                }
            });
        sorted.forEach(Family::prettyFormat);
    }
    public void displayFamiliesBiggerThen(){
        System.out.println("enter number");
        int numb = scanner.nextInt();
        getFamiliesBiggerThen(numb);
    }
    public void getFamiliesLessThen(int i) {
        List <Family> sorted = new ArrayList<>();
        familyService.getAllFamilies().forEach(x -> {
            if(x.counterFamily() < i) {
                sorted.add(x);
            }
        });
        sorted.forEach(Family::prettyFormat);
    }
    public void displayFamiliesLessThen(){
        System.out.println("enter number");
        int numb = scanner.nextInt();
        getFamiliesLessThen(numb);
    }
    public void countFamiliesWithMemberNumber(int i) {
        List <Family> sorted = new ArrayList<>();
        familyService.getAllFamilies().forEach(x -> {
            if(x.counterFamily() == i) {
                sorted.add(x);
            }
        });
        System.out.println("Families With Member Number " + i + " person is " + sorted.size());
    }

    public void displayFamiliesWithMemberNumber(){
        System.out.println("enter number");
        int numb = scanner.nextInt();
        familyService.countFamiliesWithMemberNumber(numb);
    }
    public void createNewFamily(Man man, Woman woman) {
        Family family = new Family();
        familyService.saveFamily(family);
        family.setMother(woman);
        family.setFather(man);
    }
    public void menuCreateNewFamily(){
        System.out.println("Enter mother name");
        String nameMother = scanner.nextLine();
        System.out.println("Enter mother surname");
        String surnameMother = scanner.nextLine();
        System.out.println("Enter mother's year of birth (YYYY)");
        int yearBirthMother = scanner.nextInt();
        System.out.println("Enter mother's birth month (MM)");
        int monthBirthMother = scanner.nextInt();
        System.out.println("Enter mother's birth day (DD)");
        int dayBirthMother = scanner.nextInt();
        System.out.println("Enter mother's iq");
        int iqMother = scanner.nextInt();

        System.out.println("Enter father name");
        String nameFather = scanner.nextLine();
        System.out.println("Enter surfather name");
        String surnameFather = scanner.nextLine();
        System.out.println("Enter father's year of birth (YYYY)");
        int yearBirthFather = scanner.nextInt();
        System.out.println("Enter father's birth month (MM)");
        int monthBirthFather = scanner.nextInt();
        System.out.println("Enter father's birth day (DD)");
        int dayBirthFather = scanner.nextInt();
        System.out.println("Enter father's iq");
        int iqFather = scanner.nextInt();

        ArrayList<Integer> birthdayMother = new ArrayList<>();
        birthdayMother.add(dayBirthMother);
        birthdayMother.add(monthBirthMother);
        birthdayMother.add(yearBirthMother);
        String birthdayMotherStr = birthdayMother.stream().map(Object::toString).collect(Collectors.joining("/"));

        ArrayList<Integer> birthdayFather = new ArrayList<>();
        birthdayFather.add(dayBirthFather);
        birthdayFather.add(monthBirthFather);
        birthdayFather.add(yearBirthFather);
        String birthdayFatherStr = birthdayFather.stream().map(Object::toString).collect(Collectors.joining("/"));

        familyService.createNewFamily(new Man(nameFather, surnameFather, birthdayFatherStr, iqFather),
                new Woman(nameMother, surnameMother, birthdayMotherStr, iqMother));
    }
    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }
    public void deleteFamilyByIndexMany(){
        System.out.println("enter number");
        int numb = scanner.nextInt();
        familyService.deleteFamilyByIndex(numb);
    }
    public void bornChild(Family family, String name) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        for (int j = 0; j<familyService.getAllFamilies().size(); j++){
            if(familyService.getAllFamilies().get(j) == family)
            {familyService.getAllFamilies().get(j).getChildren().add(new Human(name,
                    getAllFamilies().get(j).getFather().getSurname(), dateFormat.format(date), 0));}

        }
    }
    public void adoptChild(int index, Human human) {
        familyService.getFamilyById(index).getChildren().add(human);
    }

    public void menuAdoptChild(){
        System.out.println("Enter family ID");
        int index = scanner.nextInt();
        int indexLimit = familyService.getAllFamilies().size();
        if(index > indexLimit-1){
            System.out.println("Create a new family first");
            printMenu();
        }
        System.out.println("Enter child`s name");
        String name = scanner.nextLine();
        System.out.println("Enter child`s surname");
        String surname = scanner.nextLine();
        System.out.println("Enter child's year of birth (YYYY)");
        int yearBirthChild = scanner.nextInt();
        System.out.println("Enter child's birth month (MM)");
        int monthBirthChild = scanner.nextInt();
        System.out.println("Enter child's birth day (DD)");
        int dayBirthChild = scanner.nextInt();
        System.out.println("Enter child's iq");
        int iqChild = scanner.nextInt();

        ArrayList<Integer> birthdayChild = new ArrayList<>();
        birthdayChild.add(dayBirthChild);
        birthdayChild.add(monthBirthChild);
        birthdayChild.add(yearBirthChild);
        String birthdayChildStr = birthdayChild.stream().map(Object::toString).collect(Collectors.joining("/"));

        familyService.adoptChild(index, new Human(name, surname, birthdayChildStr, iqChild));
    }



    public void deleteChildrenOlderThen(int i) {
        familyService.getAllFamilies().forEach(x ->{
            try {
                x.getChildren().forEach(y ->{
                    if(y.getAge() > i){
                        x.getChildren().remove(y);
                    }
                });
            }
            catch(Throwable e)
            {
//                System.out.println(e.getMessage());
            }
        });
    }
    public void menuDeleteChildrenOlderThen(){
        System.out.println("enter number");
        int numb = scanner.nextInt();
        familyService.deleteChildrenOlderThen(numb);
    }
    public int count() {
        if (this.familyService == null){
            return 0;
        }
        else
            return familyService.getAllFamilies().size();
    }
    public Family getFamilyById(int id) {
        return familyService.getAllFamilies().get(id);
    }
    public void getPets(int index) {
        System.out.println(familyService.getAllFamilies().get(index).getHomePets());
    }

    public void addPet(int index, Pet pet) {
        familyService.getAllFamilies().get(index).getHomePets().add(pet);
    }

    public void menuBornChild(){
        System.out.println("Enter family ID");
        int index = scanner.nextInt();
        int indexLimit = familyService.getAllFamilies().size();
        if(index > indexLimit-1){
            System.out.println("Create a new family first");
            printMenu();
        }
        System.out.println("Enter child name");
        String name = scanner.nextLine();
        familyService.bornChild(familyService.getFamilyById(index), name);
    }

    public String birthdayMaker(){
        int day = 1 + (int)(Math.random()*31);
        int month = 1 + (int)(Math.random()*12);
        int year = 1990 + (int)(Math.random()*32);

        ArrayList<Integer> birthdayChild = new ArrayList<>();
        birthdayChild.add(day);
        birthdayChild.add(month);
        birthdayChild.add(year);
        String birthdayStr = birthdayChild.stream().map(Object::toString).collect(Collectors.joining("/"));

        return birthdayStr;
    }

    public int iqMaker(){
        return 1 + (int)(Math.random()*99);
    }

    public String stringMaker(){
            int length = 5;
            Random r = new Random();
            return r.ints(58, 122)
                    .filter(i -> (i < 57 || i > 65) && (i < 90 || i > 97))
                    .mapToObj(i -> (char) i)
                    .limit(length)
                    .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                    .toString();
            }

    public void FamilyMaker(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        for (int i = 0; i<4; i++){
            familyService.createNewFamily(new Man(stringMaker(), stringMaker(), birthdayMaker(), iqMaker()),
                    new Woman(stringMaker(), stringMaker(), birthdayMaker(),iqMaker()));
            for (int j =0; j < 4; j++){
                familyService.getFamilyById(i).getChildren().add(new Human(stringMaker(),
                        getAllFamilies().get(j).getFather().getSurname(), dateFormat.format(date), 0));
            }
        }
    }

    public void editFamily(){
        while (true){
            System.out.println("1. Born child");
            System.out.println("2. Adopt child");
            System.out.println("3. Go to main menu");

            while (!scanner.hasNextInt()) {
                System.out.println("Input again");
                scanner.nextLine();
            }

            int choise = scanner.nextInt();
            scanner.nextLine();

            switch (choise){
                case 1:
                    menuBornChild();
                    break;
                case 2:
                    menuAdoptChild();
                    break;
                case 3:
                    printMenu();
                    break;
                default:
                    System.out.println("Input again");
            }
        }
    }
    public void printMenu() {
        while (true) {
            System.out.println("1. Create test bases");
            System.out.println("2. Display the entire list of families");
            System.out.println("3. Display a list of families where the number of people is greater than the specified");
            System.out.println("4. Display a list of families where the number of people is less than a given number");
            System.out.println("5. Count the number of families where the number of members is");
            System.out.println("6. Create a new family");
            System.out.println("7. Delete a family by family index in the general list");
            System.out.println("8. Edit family by family index in the general list");
            System.out.println("9. Delete all children older than age");
            System.out.println("10. Exit");

            while (!scanner.hasNextInt()) {
                System.out.println("Input again");
                scanner.nextLine();
            }

            int choise = scanner.nextInt();
            scanner.nextLine();

            switch (choise) {
                case 1:
                    familyService.familyMaker();
                    break;
                case 2:
                    familyService.displayAllFamilies();
                    break;
                case 3:
                    familyService.displayFamiliesBiggerThen();
                    break;
                case 4:
                    familyService.displayFamiliesLessThen();
                    break;
                case 5:
                    familyService.displayFamiliesWithMemberNumber();
                    break;
                case 6:
                    familyService.menuCreateNewFamily();
                    break;
                case 7:
                    familyService.deleteFamilyByIndexMany();
                    break;
                case 8:
                    editFamily();
                    break;
                case 9:
                    familyService.menuDeleteChildrenOlderThen();
                    break;
                case 10:
                    System.out.println("Good bay!");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Input again");
            }
        }
    }
}