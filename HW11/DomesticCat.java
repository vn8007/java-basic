package HW11;

public class DomesticCat extends Pet implements Foul {

    private Specie specie = Specie.DOMESTICCAT;

    public Specie getSpecies() {
        return specie;
    }
    void respond() {
        System.out.println("Hello. My name is " + getNickname() + ". I miss you!");
    }
    @Override
    public void foul(){
        System.out.println("Need to cover tracks well....");
    }
}
