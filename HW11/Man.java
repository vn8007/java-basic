package HW11;

final public class Man extends Human {
    public Man(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public void repairCar(){
        System.out.println("repairCar");
    }

    public void greetPet() {
        System.out.println("Hay");
    }

}