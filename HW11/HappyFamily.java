package HW11;

import HW11.layered.controller.FamilyController;
import HW11.layered.dao.CollectionFamilyDao;
import HW11.layered.service.FamilyService;
import HW11.layered.service.FamilyServiceDefault;

public class HappyFamily {
    public static void main(String[] args)  {
        FamilyService familyService = new FamilyServiceDefault();
        FamilyController familyController = new FamilyController();
        familyController.setFamilyService(familyService);
        CollectionFamilyDao familyDao = new CollectionFamilyDao();
        familyService.setCollectionFamilyDao(familyDao);
        familyController.printMenu();
    }
}