package HW8.layered.controller;

import HW8.layered.domain.Human;
import HW8.layered.domain.Man;
import HW8.layered.domain.Pet;
import HW8.layered.domain.Woman;
import HW8.layered.domain.Family;
import HW8.layered.service.FamilyService;

import java.util.List;

public class FamilyController {
    private FamilyService familyService;

    public void setFamilyService(FamilyService familyService){
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }
    public void displayAllFamilies() {
        System.out.println( this.familyService == null? "No family" : familyService.getAllFamilies());
    }
    public void createNewFamily(Man man, Woman woman) {
        Family family = new Family();
        familyService.saveFamily(family);
        family.setMother(woman);
        family.setFather(man);
    }
    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }
    public void bornChild(Family family, String name) {
        for (int j = 0; j<familyService.getAllFamilies().size(); j++){
            if(familyService.getAllFamilies().get(j) == family)
            {familyService.getAllFamilies().get(j).getChildren().add(new Human(name));}
        }
    }
    public void adoptChild(Family family, Human human) {
        family.getChildren().remove(human);
    }
    public void deleteChildrenOlderThen(int i) {
        for (int j = 0; j<familyService.getAllFamilies().size(); j++){
            {for(int k =0; k < familyService.getAllFamilies().get(j).getChildren().size(); k++){
                if(familyService.getAllFamilies().get(j).getChildren().get(k).getAge()>i){
                    familyService.getAllFamilies().get(j).getChildren().remove(k);
                }
            }
            }
        }
    }
    public int count() {
        if (this.familyService == null){
            return 0;
        }
        else
            return familyService.getAllFamilies().size();
    }
    public Family getFamilyById(int id) {
        return familyService.getAllFamilies().get(id);
    }
    public void getPets(int index) {
        System.out.println(familyService.getAllFamilies().get(index).getHomePets());
    }

    public void addPet(int index, Pet pet) {
        familyService.getAllFamilies().get(index).getHomePets().add(pet);
    }
}
