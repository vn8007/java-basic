package HW8.layered.dao;

import HW8.layered.domain.Family;

import java.util.List;

public interface FamilyDao {
     List<Family> getAllFamilies();
     Family getFamilyByIndex(int index);
     boolean deleteFamilyByIndex(int index);
     boolean deleteFamily(Family family);
     void saveFamily(Family family);
}
