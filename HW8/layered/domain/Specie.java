package HW8.layered.domain;

public enum Specie {
    FISH, DOG, DOMESTICCAT, ROBOCAT, UNKNOWN
}
