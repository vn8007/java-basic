package HW8.layered.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Human {
    private Map<String, String> scedule = new HashMap<>();
    private String name;
    private String surname;
    private int year;
    private int age;
    private int iq;
//    private String[][] scedule;

    public Map<String, String> getScedule() {
        return scedule;
    }

    public void setScedule(Map<String, String> scedule) {
        this.scedule = scedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
//    public void setScedule(String[][] scedule) {
//        this.scedule = scedule;
//    }
//
//    public String[][] getScedule() {
//        return scedule;
//    }

    public void greetPet() {
        System.out.println("Some");
    }



//    void greetPet(){
//        System.out.println("Hello " + pet.getNickname() );
//    }
//    void describePet(){
//        System.out.println("I have " + pet.getSpecies() + ", he is " + pet.getAge() + " years old. " + (pet.getTrickLevel() > 50 ? "He is very cunning!":"He is not cunning!") );
//    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getYear() == human.getYear() &&
                getIq() == human.getIq() &&
                Objects.equals(getName(), human.getName()) &&
                Objects.equals(getSurname(), human.getSurname());
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Human() {
    }
    public Human(String name) {
        this.name = name;
    }

    public Human(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getName(), getSurname(), getYear(), getIq());
        return result;
    }
    protected void finalize() {
        System.out.println(this.name +" " + this.surname);
    }
}