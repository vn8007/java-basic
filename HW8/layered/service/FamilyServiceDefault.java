package HW8.layered.service;

import HW8.layered.domain.Human;
import HW8.layered.domain.Man;
import HW8.layered.domain.Pet;
import HW8.layered.domain.Woman;
import HW8.layered.dao.CollectionFamilyDao;
import HW8.layered.domain.Family;

import java.util.ArrayList;
import java.util.List;

public class FamilyServiceDefault implements FamilyService{
    private CollectionFamilyDao familyDao;

    @Override
    public void setCollectionFamilyDao(CollectionFamilyDao familyDao){
        this.familyDao = familyDao;
    }

    @Override
    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }
    @Override
    public void displayAllFamilies() {
        System.out.println( this.familyDao == null? "No family" : familyDao.getAllFamilies());
    }
    @Override
    public List<Family> getFamiliesBiggerThen(int i) {
        List<Family> sorted = new ArrayList<>();
        for (int j = 0; j<familyDao.getAllFamilies().size(); j++){
            if(familyDao.getAllFamilies().get(j).counterFamily() > i)
            {sorted.add(familyDao.getAllFamilies().get(j));}
        }
        return sorted;
    }
    @Override
    public List<Family> getFamiliesLessThen(int i) {
        List<Family> sorted = new ArrayList<>();
        for (int j = 0; j<familyDao.getAllFamilies().size(); j++){
            if(familyDao.getAllFamilies().get(j).counterFamily() < i)
            {sorted.add(familyDao.getAllFamilies().get(j));}
        }
        return sorted;
    }
    @Override
    public int countFamiliesWithMemberNumber(int i) {
        List<Family> sorted = new ArrayList<>();
        for (int j = 0; j<familyDao.getAllFamilies().size(); j++){
            if(familyDao.getAllFamilies().get(j).counterFamily() == i)
            {sorted.add(familyDao.getAllFamilies().get(j));}
        }
        return sorted.size();
    }
    @Override
    public void createNewFamily(Man man, Woman woman) {
        Family family = new Family();
        familyDao.saveFamily(family);
        family.setMother(woman);
        family.setFather(man);
    }
    @Override
    public void deleteFamilyByIndex(int index) {
        familyDao.deleteFamilyByIndex(index);
    }
    @Override
    public void bornChild(Family family, String name) {
        for (int j = 0; j<familyDao.getAllFamilies().size(); j++){
            if(familyDao.getAllFamilies().get(j) == family)
            {familyDao.getAllFamilies().get(j).getChildren().add(new Human(name));}
        }
    }
    @Override
    public void adoptChild(Family family, Human human) {
        family.getChildren().remove(human);
    }
    @Override
    public void deleteChildrenOlderThen(int i) {
        for (int j = 0; j<familyDao.getAllFamilies().size(); j++){
                {for(int k =0; k < familyDao.getAllFamilies().get(j).getChildren().size(); k++){
                    if(familyDao.getAllFamilies().get(j).getChildren().get(k).getAge()>i){
                        familyDao.getAllFamilies().get(j).getChildren().remove(k);
                    }
                }
            }
        }
    }
    @Override
    public int count() {
        if (this.familyDao == null){
            return 0;
        }
        else
         return familyDao.getAllFamilies().size();
    }
    @Override
    public Family getFamilyById(int id) {
        return familyDao.getAllFamilies().get(id);
    }
    @Override
    public void getPets(int index) {
        System.out.println(familyDao.getAllFamilies().get(index).getHomePets());
    }

    @Override
    public void addPet(int index, Pet pet) {
        familyDao.getAllFamilies().get(index).getHomePets().add(pet);
    }

    @Override
    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }
}
