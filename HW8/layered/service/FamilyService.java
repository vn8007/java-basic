package HW8.layered.service;

import HW8.layered.domain.Human;
import HW8.layered.domain.Man;
import HW8.layered.domain.Pet;
import HW8.layered.domain.Woman;
import HW8.layered.dao.CollectionFamilyDao;
import HW8.layered.domain.Family;

import java.util.List;

public interface FamilyService {
    void setCollectionFamilyDao(CollectionFamilyDao familyDao);
    List<Family> getAllFamilies();
    void displayAllFamilies();
    List<Family> getFamiliesBiggerThen(int i);
    List<Family> getFamiliesLessThen(int i);
    int countFamiliesWithMemberNumber(int i);
    void createNewFamily(Man man, Woman woman);
    void deleteFamilyByIndex(int index);
    void bornChild(Family family, String name);
    void adoptChild(Family family, Human human);
    void deleteChildrenOlderThen(int i);
    int count();
    Family getFamilyById(int id);
    void getPets(int index);
    void addPet(int index, Pet pet);

    void saveFamily(Family family);
}
