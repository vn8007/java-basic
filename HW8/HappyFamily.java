package HW8;

import HW8.layered.controller.FamilyController;
import HW8.layered.dao.CollectionFamilyDao;
import HW8.layered.domain.Man;
import HW8.layered.domain.Woman;
import HW8.layered.service.FamilyService;
import HW8.layered.service.FamilyServiceDefault;

public class HappyFamily {
    public static void main(String[] args) {
        FamilyService familyService = new FamilyServiceDefault();
        FamilyController familyController = new FamilyController();
        familyController.setFamilyService(familyService);
        CollectionFamilyDao familyDao = new CollectionFamilyDao();
        familyService.setCollectionFamilyDao(familyDao);

        familyController.createNewFamily(new Man(), new Woman());
        familyController.displayAllFamilies();

        System.out.println(familyController.count() + "families");

        familyController.createNewFamily(new Man(), new Woman());

        familyController.displayAllFamilies();
        System.out.println(familyController.count() + "families");
        familyController.bornChild(familyController.getFamilyById(0), "Name");
        familyController.getFamilyById(0).getChildren().get(0).setAge(12);
        familyController.bornChild(familyController.getFamilyById(0), "Nme");
        familyController.getFamilyById(0).getChildren().get(1).setAge(14);
        familyController.bornChild(familyController.getFamilyById(0), "Nme");
        familyController.bornChild(familyController.getFamilyById(1), "Nick");
        familyController.getFamilyById(1).getChildren().get(0).setAge(18);
        System.out.println(familyController.getFamilyById(0));
        System.out.println(familyController.getFamilyById(1));
        familyController.deleteChildrenOlderThen(13);
        System.out.println(familyController.getFamilyById(0));
        System.out.println(familyController.getFamilyById(1));

    }
}