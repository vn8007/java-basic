package HW6;

public class HappyFamily {
    public static void main(String[] args) {
        Family family = new Family();
        Dog dog = new Dog();
        family.setFather(new Man());
        family.getFather().setName("John");
        System.out.println(dog.getSpecies());
        dog.foul();
        dog.setNickname("Bublik");
        dog.respond();
        Fish fish = new Fish();
        System.out.println(fish.getSpecies());
        family.setMother(new Woman());
        family.getMother().setName("Anna");
        System.out.println(family.getMother().getName());
        System.out.println(family.getFather().getName());
        family.getFather().greetPet();
        family.getMother().greetPet();

        family.setChildren(new Human[0]);
        family.
                getMother().
                    setScedule(new String[][] {
                        {DayOfWeek.SUNDAY.name(),"do home work"},
                        {DayOfWeek.MONDAY.name(), "go to courses; watch a film"},
                        {DayOfWeek.TUESDAY.name(), "go to the office"},
                        {DayOfWeek.WEDNESDAY.name(), "do my exercises"},
                        {DayOfWeek.THURSDAY.name(), "drink beer"},
                        {DayOfWeek.FRIDAY.name(), "party day"},
                        {DayOfWeek.SATURDAY.name(), "seek a doctor"}
        });
        family.counterFamily(family.getChildren());
        family.addChild(new Human("Vasia", "Pupkin"));
        family.counterFamily(family.getChildren());
        family.addChild(new Human("Kolia", "Pupkin"));
        family.counterFamily(family.getChildren());
        family.deleteChild(1);
        family.counterFamily(family.getChildren());
        family.deleteChild(0);
        family.deleteChild(4);
        family.counterFamily(family.getChildren());

//        for (int j = 0; j < 100000; j++){
//            int length = 5;
//        Random r = new Random();
//        String s = r.ints(58, 122)
//                .filter(i -> (i < 57 || i > 65) && (i < 90 || i > 97))
//                .mapToObj(i -> (char) i)
//                .limit(length)
//                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
//                .toString();
//        Pet pet = new Pet(Specie.DOG, s);
//        Human human = new Human(s, s);
//        }
    }
}