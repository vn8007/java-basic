package HW6;

public class Dog extends Pet implements Foul{

    private Specie specie = Specie.DOG;

    public Specie getSpecies() {
        return specie;
    }

    void respond() {
        System.out.println("Hello. My name is " + getNickname() + ". I miss you!");
    }
    @Override
    public void foul(){
        System.out.println("Need to cover tracks well....");
    }
}