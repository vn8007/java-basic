package HW6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FamilyTest {
    Family family1 = new Family();
    @Test
    void addChild() {
        family1.setChildren(new Human[0]);
        family1.addChild(new Human());
        assertEquals(1, family1.getChildren().length);
    }
    @Test
    void deleteChild() {
        family1.setChildren(new Human[0]);
        family1.addChild(new Human());
        family1.deleteChild(0);
        assertEquals(0, family1.getChildren().length);
    }
    @Test
    void noDeleteChild(){
        family1.setChildren(new Human[0]);
        family1.addChild(new Human());
        family1.deleteChild(1);
        assertEquals(1, family1.getChildren().length);
    }
    @Test
    void counterFamily() {
        family1.setChildren(new Human[0]);
        family1.counterFamily(family1.getChildren());

    }

    @Test
    void testToString() {
    }
}