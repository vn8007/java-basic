package HW9.layered.service;

import HW9.Human;
import HW9.Man;
import HW9.Pet;
import HW9.Woman;
import HW9.layered.dao.CollectionFamilyDao;
import HW9.layered.domain.Family;

import java.util.List;

public interface FamilyService {
    void setCollectionFamilyDao(CollectionFamilyDao familyDao);
    List<Family> getAllFamilies();
    void displayAllFamilies();
    void getFamiliesBiggerThen(int i);
    void getFamiliesLessThen(int i);
    void countFamiliesWithMemberNumber(int i);
    void createNewFamily(Man man, Woman woman);
    void deleteFamilyByIndex(int index);
    void bornChild(Family family, String name);
    void adoptChild(Family family, Human human);
    void deleteChildrenOlderThen(int i);
    int count();
    Family getFamilyById(int id);
    void getPets(int index);
    void addPet(int index, Pet pet);

    void saveFamily(Family family);
}
