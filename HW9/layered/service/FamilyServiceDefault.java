package HW9.layered.service;

import HW9.Human;
import HW9.Man;
import HW9.Pet;
import HW9.Woman;
import HW9.layered.dao.CollectionFamilyDao;
import HW9.layered.domain.Family;

import java.util.ArrayList;
import java.util.List;

public class FamilyServiceDefault implements FamilyService {
    private CollectionFamilyDao familyDao;

    @Override
    public void setCollectionFamilyDao(CollectionFamilyDao familyDao){
        this.familyDao = familyDao;
    }

    @Override
    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }
    @Override
    public void displayAllFamilies() {
        System.out.println( this.familyDao == null? "No family" : familyDao.getAllFamilies());
    }
    @Override
    public void getFamiliesBiggerThen(int i) {
        List<Family> sorted = new ArrayList<>();
        for (int j = 0; j<familyDao.getAllFamilies().size(); j++){
            if(familyDao.getAllFamilies().get(j).counterFamily() > i)
            {sorted.add(familyDao.getAllFamilies().get(j));}
        }
        System.out.println(sorted);
    }
    @Override
    public void getFamiliesLessThen(int i) {
        List<Family> sorted = new ArrayList<>();
        for (int j = 0; j<familyDao.getAllFamilies().size(); j++){
            if(familyDao.getAllFamilies().get(j).counterFamily() < i)
            {sorted.add(familyDao.getAllFamilies().get(j));}
        }
        System.out.println(sorted);
    }
    @Override
    public void countFamiliesWithMemberNumber(int i) {
        List<Family> sorted = new ArrayList<>();
        for (int j = 0; j<familyDao.getAllFamilies().size(); j++){
            if(familyDao.getAllFamilies().get(j).counterFamily() == i)
            {sorted.add(familyDao.getAllFamilies().get(j));}
        }
        System.out.println(sorted.size());
    }
    @Override
    public void createNewFamily(Man man, Woman woman) {
        Family family = new Family();
        familyDao.saveFamily(family);
        family.setMother(woman);
        family.setFather(man);
    }
    @Override
    public void deleteFamilyByIndex(int index) {
        familyDao.deleteFamilyByIndex(index);
    }
    @Override
    public void bornChild(Family family, String name) {
        for (int j = 0; j<familyDao.getAllFamilies().size(); j++){
            if(familyDao.getAllFamilies().get(j) == family)
            {familyDao.getAllFamilies().get(j).getChildren().add(new Human(name));}
        }
    }
    @Override
    public void adoptChild(Family family, Human human) {
        family.getChildren().remove(human);
    }
    @Override
    public void deleteChildrenOlderThen(int i) {
        for (int j = 0; j<familyDao.getAllFamilies().size(); j++){
                {for(int k =0; k < familyDao.getAllFamilies().get(j).getChildren().size(); k++){
                    if(familyDao.getAllFamilies().get(j).getChildren().get(k).getAge()>i){
                        familyDao.getAllFamilies().get(j).getChildren().remove(k);
                    }
                }
            }
        }
    }
    @Override
    public int count() {
        if (this.familyDao == null){
            return 0;
        }
        else
         return familyDao.getAllFamilies().size();
    }
    @Override
    public Family getFamilyById(int id) {
        return familyDao.getAllFamilies().get(id);
    }
    @Override
    public void getPets(int index) {
        System.out.println(familyDao.getAllFamilies().get(index).getHomePets());
    }

    @Override
    public void addPet(int index, Pet pet) {
        familyDao.getAllFamilies().get(index).getHomePets().add(pet);
    }

    @Override
    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }
}
