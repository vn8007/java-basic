package HW9.layered.domain;

import HW9.Human;
import HW9.Man;
import HW9.Pet;
import HW9.Woman;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Family {
    private Woman mother;
    private Man father;
    private List<Human> children = new ArrayList<>();
    private Set<Pet> homePets = new HashSet<>();

    public Set<Pet> getHomePets() {
        return homePets;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }


//    public void addChild(Human human){
//        Human[] data = {human};
//        Human[] data2 = new Human[data.length + children.length];
//        System.arraycopy(children,0,data2,0, children.length);
//        System.arraycopy(data, 0, data2, children.length, data.length);
//        children = data2;
//        System.out.println(Arrays.toString(children));
//    }

//    public void deleteChild(int index){
//        if(index >= children.length){
//            System.out.println("Incorrect index");
//        }
//        else
//        {for(int j = 0; j < children.length; j++) {
//            if (children[j] == children[index]) {
//                System.out.println("Child " + children[index].getName() +" deleted");
//                Human[] data = new Human[children.length - 1];
//                int counter = 0;
//                for (int i = 0; i < children.length; i++) {
//                    if (children[i] == children[index]) {
//                        continue;
//                    }
//                    data[counter] = children[i];
//                    counter++;
//                }
//                children = data;
//                if (children.length == 0){
//                    System.out.println("You do not have kids!");
//                }
//                else {
//                    System.out.println(Arrays.toString(children));
//                }}
//        }}
//    }
    public int counterFamily(){
        int countFamily;
        countFamily = 2 + getChildren().size();
        return countFamily;
    }


    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", homePets=" + homePets +
                '}';
    }

    public void finalize (){
        System.out.println("family");
    }

}