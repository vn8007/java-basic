package HW9.layered.dao;

import HW9.layered.domain.Family;

import java.util.List;

public interface FamilyDao {
     List<Family> getAllFamilies();
     void getFamilyByIndex(int index);
     boolean deleteFamilyByIndex(int index);
     boolean deleteFamily(Family family);
     void saveFamily(Family family);
}
