package HW9;

import HW9.layered.controller.FamilyController;
import HW9.layered.dao.CollectionFamilyDao;
import HW9.layered.service.FamilyService;
import HW9.layered.service.FamilyServiceDefault;

import java.text.ParseException;

public class HappyFamily {
    public static void main(String[] args)  {
        FamilyService familyService = new FamilyServiceDefault();
        FamilyController familyController = new FamilyController();
        familyController.setFamilyService(familyService);
        CollectionFamilyDao familyDao = new CollectionFamilyDao();
        familyService.setCollectionFamilyDao(familyDao);

        familyController.createNewFamily(new Man("John", "05/09/1984"), new Woman());
        familyController.displayAllFamilies();

        System.out.println(familyController.count() + "families");

        familyController.createNewFamily(new Man("Den", "07/06/1999"), new Woman());
        familyController.getFamilyById(0).getFather().describeAge();
        System.out.println(familyController.getFamilyById(0).getFather().toString());

        familyController.displayAllFamilies();
        System.out.println(familyController.count() + "families");
        familyController.bornChild(familyController.getFamilyById(0), "Name");
        familyController.getFamilyById(0).getChildren().get(0).setAge(12);
        familyController.bornChild(familyController.getFamilyById(0), "Nme");
        familyController.getFamilyById(0).getChildren().get(1).setAge(14);
        familyController.bornChild(familyController.getFamilyById(0), "Nme");
        familyController.bornChild(familyController.getFamilyById(1), "Nick");
        familyController.getFamilyById(1).getChildren().get(0).setAge(18);
        System.out.println(familyController.getFamilyById(0));
        System.out.println(familyController.getFamilyById(1));
        familyController.deleteChildrenOlderThen(13);
        System.out.println(familyController.getFamilyById(0));
        System.out.println(familyController.getFamilyById(1));

    }
}