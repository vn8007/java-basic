package HW9;

final public class Man extends Human {
    public Man(String name, String birthDate) {
        super(name, birthDate);
    }

    public void repairCar(){
        System.out.println("repairCar");
    }

    public void greetPet() {
        System.out.println("Hay");
    }

}