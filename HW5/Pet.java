package HW5;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private Specie species;
    private  String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Specie getSpecies() {
        return species;
    }

    public void setSpecies(Specie species) {
        this.species = species;
    }
    public void setNickname(String nickname){
        this.nickname = nickname;
    }
    public String getNickname(){
        return nickname;
    }
    public void setAge(int age){
        this.age = age;
    }
    public int getAge(){
        return age;
    }
    public void setTrickLevel(int trickLevel){
        this.trickLevel = trickLevel;
    }
    public int getTrickLevel(){
        return trickLevel;
    }
    public void setHabits(String[] habits){
        this.habits = habits;
    }
    public String[] getHabits(){
        return habits;
    }

    void eat(){
        System.out.println("I am eating!");
    }
    void respond(){
        System.out.println("Hello. My name is "+ this.nickname +". I miss you!");
    }
    void foul(){
        System.out.println("Need to cover tracks well....");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;
        Pet pet = (Pet) o;
        return getTrickLevel() == pet.getTrickLevel() &&
                Objects.equals(getSpecies(), pet.getSpecies()) &&
                Objects.equals(getNickname(), pet.getNickname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSpecies(), getNickname(), getTrickLevel());
    }

    @Override
    public String toString() {
        return species + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    public Pet(){};


    public Pet(Specie species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(Specie species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }


    public void finalize (){
        System.out.println(this.nickname +" " + this.species);
    }

}