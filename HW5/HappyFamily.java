package HW5;

public class HappyFamily {
    public static void main(String[] args) {
        Family family = new Family();
        family.setMother(new Human());
        family.getMother().setName("Anna");
        family.setFather(new Human("John", "Pupkin"));
        family.setChildren(new Human[0]);
        family.setPet(new Pet());
        family.getPet().setSpecies(Specie.DOG);
        family.getMother().setScedule(new String[][] {
                {DayOfWeek.SUNDAY.name(),"do home work"},
                {DayOfWeek.MONDAY.name(), "go to courses; watch a film"},
                {DayOfWeek.TUESDAY.name(), "go to the office"},
                {DayOfWeek.WEDNESDAY.name(), "do my exercises"},
                {DayOfWeek.THURSDAY.name(), "drink beer"},
                {DayOfWeek.FRIDAY.name(), "party day"},
                {DayOfWeek.SATURDAY.name(), "seek a doctor"}
        });
        family.counterFamily(family.getChildren());
        family.addChild(new Human("Vasia", "Pupkin"));
        family.counterFamily(family.getChildren());
        family.addChild(new Human("Kolia", "Pupkin"));
        family.counterFamily(family.getChildren());
        family.deleteChild(1);
        family.counterFamily(family.getChildren());
        family.deleteChild(0);
        family.deleteChild(4);
        family.counterFamily(family.getChildren());

//        for (int j = 0; j < 100000; j++){
//            int length = 5;
//        Random r = new Random();
//        String s = r.ints(58, 122)
//                .filter(i -> (i < 57 || i > 65) && (i < 90 || i > 97))
//                .mapToObj(i -> (char) i)
//                .limit(length)
//                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
//                .toString();
//        Pet pet = new Pet(Specie.DOG, s);
//        Human human = new Human(s, s);
//        }
    }
}