package HW2;

import java.util.Scanner;

public class SolidGame
{
    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble!");
        char [][] field = new char[6][6];
        int xRandom;
        int yRandom;
        xRandom = (int)((Math.random()*(4+1)) + 1);
        yRandom = (int)((Math.random()*(4+1)) + 1);
//        System.out.println(xRandom + "  " + yRandom);
        buildMatrix(field);
        shot(xRandom,yRandom, field);
        drawMatrix(field);
    }
    public static void buildMatrix(char[][] matrix){
        String str;
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix[i].length; j++){
                str = Integer.toString(j);
                matrix[i][j] = str.charAt(0);
            }
        }
        for (int i = 1; i < matrix.length; i++){
            for (int j = 0; j < matrix[i].length; j++){
                str = Integer.toString(i);
                matrix[i][j] = str.charAt(0);
            }
        }
        for (int i = 1; i < matrix.length; i++){
            for (int j = 1; j < matrix[i].length; j++){
                matrix[i][j] = '-';
            }
        }
    }
    public static void drawMatrix (char[][] matrix){
        for(int i=0; i<matrix.length; i++){
            for(int j=0; j<matrix[i].length; j++){
                System.out.print(" "+matrix[i][j]+ " |");
            }
            System.out.println(" ");
        }
    }
    public static void shot(int xRandom, int yRandom, char[][] matrix){
        while (true){
            Scanner scan = new Scanner(System.in);
            int x;
            while(true) {
                System.out.println("Enter vertical point");
                x = scan.nextInt();
                if ((x < 1) | (x > 5)) {
                    System.out.println("Data is not correct!");
                    continue;
                }
                break;
            }
            int y;
            while(true) {
                System.out.println("Enter horizontal point");
                y = scan.nextInt();
                if ((y < 1) | (y > 5)) {
                    System.out.println("Data is not correct!");
                    continue;
                }
                break;
            }
            if((x!=xRandom)|(y!=yRandom)){
                matrix[x][y] = '*';
                drawMatrix(matrix);
                continue;
            }
            matrix[xRandom][yRandom] = 'X';
            System.out.println("You have won!");
            break;
        }
    }
}