package Step.layered.controller;

import Step.layered.domain.Flight;
import Step.layered.domain.User;
import Step.layered.service.FlightService;
import Step.layered.service.UserService;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class FlightController {
    private FlightService flightService;
    private UserService userService;
    private Scanner scanner = new Scanner(System.in);

    public void setFlightService(FlightService flightService){
        this.flightService = flightService;
    }

    public void setUserService(UserService userService){this.userService = userService;}

    public List<Flight> getAllFlights(){
        return flightService.getAllFlights();
    }

    private void displayFlightById(){
        System.out.println("Enter fligthID");
        while (!scanner.hasNextInt()) {
            System.out.println("Input again");
            scanner.nextLine();
        }
        int id = scanner.nextInt();
        if (flightService.getFlightByID(id).size() == 0){
            System.out.println("No flights, incorrect ID");
        }
        flightService.getFlightByID(id).forEach(u -> System.out.println(u.toInformationString()));
    }

    private void displayAllFlight(){
        flightService.getAllFlights().forEach(u -> System.out.println(u.toString()));
    }

    private void deleteUserById(){
        System.out.println("Enter ID");
        while (!scanner.hasNextInt()) {
            System.out.println("Input again");
            scanner.nextLine();
        }
        int id = scanner.nextInt();
        for (int i = 0; i < userService.getAllUsers().size(); i++)
        {if(userService.getAllUsers().get(i).getId() == id)
        {try {
            flightService.getById(userService.getAllUsers().get(i).getFlight().getFlightID()).setNumberOfSeats(
                    flightService.getById(userService.getAllUsers().get(i).getFlight().getFlightID()).getNumberOfSeats() +
                            userService.getAllUsers().get(i).getSeats()
            );
        }
        catch (NullPointerException e){
//            System.out.println("Ok");
        }

        userService.deleteUser(userService.getAllUsers().get(i));
            System.out.println("Booked flight ID " + id + " deleted.");
        }
        }
    }

    private void displayMyFlight(){
        if (userService.getAllUsers().size() > 0)
        {   System.out.println("Enter passenger name");
            String name = scanner.nextLine();
            System.out.println("Enter passenger surname");
            String surname = scanner.nextLine();
            userService.getUser(name, surname).forEach(u -> System.out.println(u.toString()));
            if(userService.getUser(name, surname).size() == 0){
                System.out.println("Passenger " + name+ " " + surname + " has no flights booked.");
            }
            }
        else System.out.println("No flight");
    }

    private void booking(int numb, int userNumberOfSeats){
        scanner.nextLine();
        for(int i =0; i < userNumberOfSeats; i++){
        System.out.println("Enter passenger name");
        String name = scanner.nextLine();
        System.out.println("Enter passenger surname");
        String surname = scanner.nextLine();
        int flightID = flightService.createID();
        userService.create(new User(name, surname, flightService.getById(numb) ,1, flightID));
        flightService.correctSeats(numb, 1);
        System.out.println("Flight booked for " + name + " " + surname +". Reservation number " + flightID + ".");
        }
    }

    private void findFlight() {
        try {
        System.out.println("Enter destination city");
        String destCity = scanner.nextLine().trim().toUpperCase();
        System.out.println("Enter date dd/mm/yyyy");
        String dateFlight = scanner.nextLine();
        System.out.println("How many tickets do you need to buy?");
        int userNumberOfSeats = scanner.nextInt();
        if (flightService.findUserData(destCity, dateFlight, userNumberOfSeats).size() == 0) {
                System.out.println("No data");
            } else {
                flightService.findUserData(destCity, dateFlight, userNumberOfSeats).forEach(u -> System.out.println(u.toString()));
                System.out.println("Enter flight number to book or enter 0 to exit main menu");
                int numb = scanner.nextInt();
                if (numb == 0) {
                    printMenu();
                }
                try {
                    if (flightService.getById(numb).getFlightID() == numb) {
                        booking(numb, userNumberOfSeats);
                    }
                } catch (NullPointerException e) {

                    System.out.println("Incorrect data!");
                }

            }
        } catch (InputMismatchException e) {
            System.out.println("Incorrect data");
        }
    }

    public void printMenu() {
        while (true) {
            System.out.println(" ");
            System.out.println("1. Online scoreboard");
            System.out.println("2. View flight information");
            System.out.println("3. Flight search and booking");
            System.out.println("4. Cancel booking");
            System.out.println("5. My flights");
            System.out.println("6. Exit");

            while (!scanner.hasNextInt()) {
                System.out.println("Input again");
                scanner.nextLine();
            }

            int choise = scanner.nextInt();
            scanner.nextLine();

            switch (choise) {
                case 1:
                    displayAllFlight();
                    break;
                case 2:
                    displayFlightById();
                    break;
                case 3:
                    findFlight();
                    break;
                case 4:
                    deleteUserById();
                    break;
                case 5:
                    displayMyFlight();
                    break;
                case 6:
                    System.out.println("Good bay!");
                    System.exit(0);
                    break;

                default:
                    System.out.println("Input again");
            }
        }
    }
}