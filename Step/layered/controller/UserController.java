package Step.layered.controller;

import Step.layered.domain.Flight;
import Step.layered.domain.User;
import Step.layered.service.UserService;

import java.util.List;

public class UserController {
    private UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public boolean addNewUser(String name, String surname, Flight flight, int seats, int id) {
        User newUser = new User(name, surname, flight,seats, id);
        if (userService.contains(newUser)) {
            System.out.println("User already exists");
            return false;
        }
        userService.create(newUser);
        return true;
    }

    public void displayAllUser(){
        userService.getAllUsers().forEach(u-> System.out.println(u.toString()));
    }

}
