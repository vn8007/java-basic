package Step.layered;


import Step.layered.controller.FlightController;
import Step.layered.controller.UserController;
import Step.layered.dao.FlightDao;
import Step.layered.dao.UserInMemoryDao;
import Step.layered.service.FlightService;
import Step.layered.service.UserService;

public class booking {
    public static void main(String[] args) {
        FlightService flightService = new FlightService();
        FlightController flightController = new FlightController();
        flightController.setFlightService(flightService);
        FlightDao flightDao = new FlightDao();
        flightService.setFlightDao(flightDao);
        UserService userService = new UserService();
        UserController userController = new UserController();
        userController.setUserService(userService);
        flightController.setUserService(userService);
        UserInMemoryDao userInMemoryDao = new UserInMemoryDao();
        userService.setUserDao(userInMemoryDao);
        flightController.printMenu();
    }
}