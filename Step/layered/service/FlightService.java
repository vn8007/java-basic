package Step.layered.service;

import Step.layered.dao.FlightDao;
import Step.layered.domain.Flight;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FlightService {

    private FlightDao flightDao;

    public void setFlightDao(FlightDao flightDao){
        this.flightDao = flightDao;
    }

    public List<Flight> getAllFlights(){
        return flightDao.getAllFlights();
    }

    public List<Flight> getFlightByID(int id){
        List<Flight> findID = new ArrayList<>();
        for (int i = 0; i < getAllFlights().size(); i++){
            if(getAllFlights().get(i).getFlightID() == id){
                findID.add(getAllFlights().get(i));
            }
        }
        return findID;
    }

    public Flight getById(int numb){
        for (int i = 0; i < getAllFlights().size(); i++){
            if (getAllFlights().get(i).getFlightID() == numb){
                return getAllFlights().get(i);
            }
        }
        return null;
    }

    public List<Flight> findUserData(String city, String date, int numb){
        List<Flight> findUser = new ArrayList<>();
        for (int i = 0; i < getAllFlights().size(); i++){
            if(getAllFlights().get(i).getDestination().equals(city) & getAllFlights().get(i).getNumberOfSeats() > numb &
                    new SimpleDateFormat("dd/MM/yyyy").format(new Date(getAllFlights().get(i).getFligthDate())).equals(date)){
                findUser.add(getAllFlights().get(i));
            }
        }
        return findUser;
    }

    public void correctSeats(int id, int i){
        getById(id).setNumberOfSeats(getById(id).getNumberOfSeats()-i);
    }

    public int createID(){
        return flightDao.createFlightID();
    }
}