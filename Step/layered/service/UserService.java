package Step.layered.service;

import Step.layered.dao.UserDao;
import Step.layered.domain.User;

import java.util.ArrayList;
import java.util.List;

public class UserService {
    private UserDao userDao;

    public void setUserDao(UserDao userInMemoryDao) {
        this.userDao = userInMemoryDao;
    }

    public void create(User user) {
        userDao.create(user);
    }

    public boolean contains(User user) {
        return userDao.contains(user);
    }

    public List<User> getAllUsers(){
        return userDao.findAll();
    }

    public List<User> getUser(String name, String surname){
        List<User> find = new ArrayList<>();
        for (int i = 0; i < userDao.findAll().size(); i ++){
            if(userDao.findAll().get(i).getName().equals(name) & userDao.findAll().get(i).getSurname().equals(surname)){
                find.add(userDao.findAll().get(i));
            }
        }
        return find;
    };

    public void deleteUser(User user){
        userDao.delete(user);
    }
}