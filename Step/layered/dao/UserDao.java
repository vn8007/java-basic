package Step.layered.dao;

import Step.layered.domain.User;

import java.util.List;

public interface UserDao {
    void create(User user);

    List<User> findAll();

    boolean contains(User user);
    void delete(User user);

}
