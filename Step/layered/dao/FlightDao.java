package Step.layered.dao;

import Step.layered.domain.Cities;
import Step.layered.domain.Flight;

import java.io.*;
import java.util.*;


public class FlightDao{
    private static final String FILE_NAME = "flight.obj";
    private File file = new File(FILE_NAME);
    private List<Flight> flightDao = new ArrayList<>();

        public FlightDao() {
        if (file.exists()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                this.flightDao = (List<Flight>)ois.readObject();
                Date current = new Date();
                long time = current.getTime();
                for (int i = 0; i<flightDao.size(); i++){
                    if(flightDao.get(i).getFligthDate() < time){
                        delete(flightDao.get(i));
                    }
                    Collections.sort(flightDao);
                }
                if(flightDao.size() < 15){
                    createFlights();
                    Collections.sort(flightDao);
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            try {
                file.createNewFile();
                    createFlights();
                    Collections.sort(flightDao);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

        public int createFlightID(){
            int upperBound = 999;
            int lowerBound = 100;
            return lowerBound + (int)(Math.random() * ((upperBound - lowerBound) + 1));
        }

        private long createDate(){
            Date current = new Date();
            long startTime = current.getTime();
            long finTime = current.getTime() + (3600000 * 24);
            long time = startTime + (long) (Math.random() * ((finTime - startTime) + 1));
            return time;
        }

        private void createFlights(){
            ArrayList<Cities> citiesDestination = new ArrayList<>(Arrays.asList(Cities.values()));
            for (int i = 0; i < 3; i++){
            for (Cities cities : citiesDestination) {
                flightDao.add(new Flight(cities.toString(), createFlightID(), 100, createDate()));
                if (file.exists()) {
                    try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
                        oos.writeObject(this.flightDao);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }}
        }

        private void delete(Flight flight) {
            flightDao.remove(flight);
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
                oos.writeObject(this.flightDao);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public List<Flight> getAllFlights() {
            return new ArrayList<>(flightDao);
        }
}