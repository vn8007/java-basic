package Step.layered.domain;

import java.io.Serial;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Flight implements Serializable, Comparable<Flight> {
    @Serial
    private static final long serialVersionUID = 1L;
    private String destination;
    private int  flightID;
    private int numberOfSeats;
    private long fligthDate;

    public int getFlightID() {
        return flightID;
    }

    public String getDestination() {
        return destination;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public long getFligthDate() {
        return fligthDate;
    }

    public Flight(String destination, int flightID, int numberOfSeats, long fligthDate) {
        this.destination = destination;
        this.flightID = flightID;
        this.numberOfSeats = numberOfSeats;
        this.fligthDate = fligthDate;
    }

    @Override
    public String toString() {
        return "Flight from Kyiv to " +
                 destination + ", flight number " + flightID +", number Of Seats " + numberOfSeats +
                ", flight Date " + new SimpleDateFormat("dd/MM/yyyy").format(new Date(fligthDate))  +
                ", flight Time " + new SimpleDateFormat("HH:mm").format(new Date(fligthDate));
    }

    public String toInformationString(){
        return "Flight number " + flightID + "\n" +
                "Departure city Kyiv " + "\n" +
                "Destination city " + destination + "\n" +
                "Free places for booking " + numberOfSeats + "\n" +
                "Departure date " + new SimpleDateFormat("dd/MM/yyyy").format(new Date(fligthDate)) + "\n" +
                "Departure time " + new SimpleDateFormat("HH:mm").format(new Date(fligthDate)) + "\n" +
                "Have a good flight!";
    }

    @Override
    public int compareTo(Flight o) {
        return (int)(this.getFligthDate() - o.getFligthDate());
    }
}
