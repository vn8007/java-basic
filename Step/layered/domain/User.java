package Step.layered.domain;

import java.io.Serial;
import java.io.Serializable;

public class User implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private String name;
    private String surname;
    private Flight flight;
    private int id;
    private int seats;

    public User(String name, String surname, Flight flight, int seats, int id) {
        this.name = name;
        this.surname = surname;
        this.flight = flight;
        this.seats = seats;
        this.id = id;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", flight=" + flight +
                ", id=" + id +
                ", seats=" + seats +
                '}';
    }

    public String toStringSmall(){
        return "Flight booked for " + name + " " + surname +". Reservation number " + id + ".";
    }
}