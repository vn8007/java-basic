package HW7;

import java.util.*;

public class Family {
    private Woman mother;
    private Man father;
    private List<Human> children = new ArrayList<>();
    private Set<Pet> homePets = new HashSet<>();

    public Set<Pet> getHomePets() {
        return homePets;
    }

    private Pet pet;
    private int countFamily;

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }


    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }


    public void addChild(Human human){
        this.children.add(human);
    }

    public void removeChild(int index){
        this.children.remove(index);
    }



//    public void addChild(Human human){
//        Human[] data = {human};
//        Human[] data2 = new Human[data.length + children.length];
//        System.arraycopy(children,0,data2,0, children.length);
//        System.arraycopy(data, 0, data2, children.length, data.length);
//        children = data2;
//        System.out.println(Arrays.toString(children));
//    }

//    public void deleteChild(int index){
//        if(index >= children.length){
//            System.out.println("Incorrect index");
//        }
//        else
//        {for(int j = 0; j < children.length; j++) {
//            if (children[j] == children[index]) {
//                System.out.println("Child " + children[index].getName() +" deleted");
//                Human[] data = new Human[children.length - 1];
//                int counter = 0;
//                for (int i = 0; i < children.length; i++) {
//                    if (children[i] == children[index]) {
//                        continue;
//                    }
//                    data[counter] = children[i];
//                    counter++;
//                }
//                children = data;
//                if (children.length == 0){
//                    System.out.println("You do not have kids!");
//                }
//                else {
//                    System.out.println(Arrays.toString(children));
//                }}
//        }}
//    }
    public void counterFamily(){
        countFamily = 2 + getChildren().size();
        System.out.println("Family consists of "+ countFamily + " people");
    }


    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                '}';
    }
    public void finalize (){
        System.out.println("family");
    }

}