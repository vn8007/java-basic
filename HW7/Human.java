package HW7;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Human {
    private Map<DayOfWeek, String> scedule = new HashMap<>();
    private String name;
    private String surname;
    private int year;
    private int iq;


    public Map<DayOfWeek, String> getScedule() {
        return scedule;
    }

    public void setScedule(Map<DayOfWeek, String> scedule) {
        this.scedule = scedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void greetPet() {
        System.out.println("Some");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getYear() == human.getYear() &&
                getIq() == human.getIq() &&
                Objects.equals(getName(), human.getName()) &&
                Objects.equals(getSurname(), human.getSurname());
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Human() {
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getName(), getSurname(), getYear(), getIq());
        return result;
    }
    protected void finalize() {
        System.out.println(this.name +" " + this.surname);
    }
}