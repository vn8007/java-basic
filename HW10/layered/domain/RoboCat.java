package HW10.layered.domain;

public class RoboCat extends Pet {

    private Specie specie = Specie.ROBOCAT;

    public Specie getSpecies() {
        return specie;
    }

    void respond() {
        System.out.println("Hello. My name is " + getNickname() + ". I miss you!");

    }
}