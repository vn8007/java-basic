package HW10.layered.domain;

public enum Specie {
    FISH, DOG, DOMESTICCAT, ROBOCAT, UNKNOWN
}
