package HW10.layered.dao;

import HW10.layered.domain.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> collectionFamilyDao = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return new ArrayList<>(collectionFamilyDao);
    }

    @Override
    public void getFamilyByIndex(int index) {
            if(index<0 || index>=collectionFamilyDao.size()){
                System.out.println("Incorrect index");
            }
            else {
                System.out.println(collectionFamilyDao.get(index));
            }
    }

    @Override

    public boolean deleteFamilyByIndex(int index) {
        if(index<0 || index>=collectionFamilyDao.size()) {
            return false;
        }
        else {
            collectionFamilyDao.remove(index);
            return true;
        }
    }

    @Override
    public boolean deleteFamily(Family family) {
        if(collectionFamilyDao.contains(family)){
            collectionFamilyDao.remove(family);
            return true;
        }
        else
            return false;
    }

    @Override
    public void saveFamily(Family family) {
        if(!collectionFamilyDao.contains(family))
        {collectionFamilyDao.add(family);}
        else System.out.println("Family already in collection");
    }
}
