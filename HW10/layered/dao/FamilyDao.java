package HW10.layered.dao;

import HW10.layered.domain.Family;

import java.util.List;

public interface FamilyDao {
     List<Family> getAllFamilies();
     void getFamilyByIndex(int index);
     boolean deleteFamilyByIndex(int index);
     boolean deleteFamily(Family family);
     void saveFamily(Family family);
}
