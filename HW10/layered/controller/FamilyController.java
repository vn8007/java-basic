package HW10.layered.controller;

import HW10.layered.domain.Human;
import HW10.layered.domain.Man;
import HW10.layered.domain.Pet;
import HW10.layered.domain.Woman;
import HW10.layered.domain.Family;
import HW10.layered.service.FamilyService;

import java.util.ArrayList;
import java.util.List;

public class FamilyController {
    private FamilyService familyService;

    public void setFamilyService(FamilyService familyService){
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }
    public void displayAllFamilies() {
        familyService.getAllFamilies().stream().forEach(System.out::println);
    }
    public void createNewFamily(Man man, Woman woman) {
        Family family = new Family();
        familyService.saveFamily(family);
        family.setMother(woman);
        family.setFather(man);
    }
    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }
    public void bornChild(Family family, String name) {
        for (int j = 0; j<familyService.getAllFamilies().size(); j++){
            if(familyService.getAllFamilies().get(j) == family)
            {familyService.getAllFamilies().get(j).getChildren().add(new Human(name));}
        }
    }
    public void adoptChild(Family family, Human human) {
        family.getChildren().remove(human);
    }
    public void deleteChildrenOlderThen(int i) {
        familyService.getAllFamilies().forEach(x ->{
            try {
                x.getChildren().forEach(y ->{
                    if(y.getAge() > i){
                        x.getChildren().remove(y);
                    }
                });
            }
            catch(Throwable e)
            {
//                System.out.println(e.getMessage());
            }
        });
    }
    public int count() {
        if (this.familyService == null){
            return 0;
        }
        else
            return familyService.getAllFamilies().size();
    }
    public Family getFamilyById(int id) {
        return familyService.getAllFamilies().get(id);
    }
    public void getPets(int index) {
        System.out.println(familyService.getAllFamilies().get(index).getHomePets());
    }

    public void addPet(int index, Pet pet) {
        familyService.getAllFamilies().get(index).getHomePets().add(pet);
    }
}
